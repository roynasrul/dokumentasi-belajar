# Queue

![queue](file/queue.gif)

Queue (Antrean) merupakan sebuah alur proses dimana tugas yang masuk terlebih dahulu akan di proses terlebih dahulu, dan tugas baru akan ditambahkan pada bagian belakang antrean (FIFO).

# Message

![message](file/message.gif)

Message (pesan) adalah sebuah data yang kirimkan oleh pengirim kepada penerima. Tergantung dari informasi dari data tersebut penerima akan melakukan pemrosesan lanjutan.

# Message Queue

![MQ](file/mq.gif)

Message Queue adalah gabungan antara keduanya (Queue dan Message). Dalam praktik nya, Pesan yang di kirim oleh pengirim (Producer) akan dimasukkan pada antrean (Queue) lalu akan dibaca oleh penerima (Consumer).

# Kegunaan Message Queue

Message Queue menyediakan cara komunikasi asynchronous, yang cocok digunakan ketika system memiliki suatu proses yang cukup panjang namun respon dari proses tersebut tidak diperlukan sesegera mungkin untuk melanjutkan proses lainnya.

Metode berkirim barang melalui Kurir, mungkin salah satu contoh yang cocok untuk kasus ini. Ketika paket sudah disampaikan ke kurir dan didaftarkan kedalam suatu proses pengiriman, pengirim tidak perlu menunggu informasi dari penerima paket, Ia hanya perlu melanjutkan tugas yang lainnya.

## Decoupled System

![decoupled](file/decoupled.jpg)

Salah satu Manfaat dari Message Queue adalah Decoupled System. Decoupled System adalah suatu keadaan dimana dua atau lebih system bisa berkomunikasi tanpa harus saling terhubung. Dalam keadaan ini, system akan sepenuhnya mandiri dan tidak bergantung dengan system lainnya.

sebagai contoh, ketika ada satu decoupled system gagal menjalankan suatu proses dari queue, maka proses baru tetap bisa ditambakan ke dalam queue dan diproses ketika systemnya sudah diperbaiki.

Jika menggunakan contoh kasus berkirim barang, akan ada kemungkinan alamat penerima paket sedang kosong sehingga petugas Kurir tidak bisa mengirimkan paket. lalu barang akan disimpan kembali oleh petugas dan akan dikirim kemudian hari dengan harapan penerima paket sudah ada di rumah.

## Scalability

![scalability](file/scalability.jpg)

Message queue juga meningkatkan skalabilitas suatu system. Karena dengan kondisi system yang terpisah maka melakukan skala ulang sangat mudah.

Sebagai contoh, ketika kita merasa skala data yang masuk sudah lebih banyak sehingga antrean data tidak memungkinkan untuk dihandle oleh satu consumer, kita hanya perlu menggandakan konsumer tersebut dan menyesuaikan jumlahnya dengan kebutuhan kita.

Untuk contoh kasus dalam kehidupan sehari-hari, mari kita berpindah dari pengiriman barang menuju restauran. ketika restauran sudah menjadi lebih besar dan terkenal maka untuk mengatasi pesanan makanan yang banyak restauran bisa menambahkan jumlah koki sehingga antrean pesanan bisa teratasi dengan baik dan tidak membuat pelanggan menunggu terlalu lama.

# Contoh Kasus

Beberapa Contoh proses yang bisa memanfaatkan queue adalah :

1. Penulisan Log Aktifitas
2. Pengiriman notifikasi email, sms.
3. Update status transaksi
4. Update status barang

dan proses lain yang tidak mentolerir adanya kehilangan data atau informasi, dan juga bisa diproses secara asyncronus.

# Contoh Implementasi

## BullMQ (NodeJS)

![bullMQ](file/bullMQ.png)

[BullMq GitHub](https://github.com/taskforcesh/bullmq)

### Persiapan

Sebelum memulai pengkodean, ada beberapa hal yang harus kita siapkan.

1. Install NodeJS dan NPM
2. Install Redis

Untuk installasi redis saya lebih suka menggunakan docker-compose. berikut isi compose file nya.

```
version: "3"

services:
  redis:
    image: "redis:5.0-alpine"
    ports:
      - "6379:6379"
    command: redis-server --requirepass default123

```

### Code

Consumer

```javascript
const { Worker } = require("bullmq");

function logger(job) {
  console.log("-------");
  console.log(job.name);
  console.log(job.data);
  console.log("-------");
}

const worker = new Worker(
  "Logger",
  async (job) => {
    if (job.name === "user-login") {
      await delay(1000);
      await logger(job);
    }
    if (job.name === "user-update") {
      await delay(1000);
      await logger(job);
    }
    if (job.name === "user-delete") {
      await delay(1000);
      await logger(job);
    }
  },
  { connection: { host: "localhost", port: "6379", password: "default123" } }
);

const delay = (time) => new Promise((res) => setTimeout(res, time));
```

Producer

```javascript
const { Queue } = require("bullmq");

const queue = new Queue("Logger", {
  connection: { host: "localhost", port: "6379", password: "default123" },
});

function produce() {
  for (let index = 0; index < 10; index++) {
    queue.add("user-login", {
      id: "af4b0d3e-90bf-4756-8c8d-f5998d8efd8e",
    });
    queue.add("user-update", {
      id: "af4b0d3e-90bf-4756-8c8d-f5998d8efd8e",
      oldData: { address: "old" },
      newData: { address: "new" },
    });
    queue.add("user-delete", {
      id: "af4b0d3e-90bf-4756-8c8d-f5998d8efd8e",
    });
  }
}

produce();
```
